def update(self: Self, choice: str, feedback: str) -> None:
    def matches_feedback(word: str, choice: str, feedback: str) -> bool:
        for i in range(len(choice)):
            if feedback[i] == 'G' and word[i] != choice[i]:
                return False
            elif feedback[i] == 'Y' and word[i] == choice[i]:
                return False
            elif feedback[i] == 'R' and choice[i] in word:
                return False
        return True

    self.choices = [w for w in self.choices if matches_feedback(w, choice, feedback)]
