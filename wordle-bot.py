import requests as rq
import random
DEBUG = False

class MMBot:
    words = [word.strip() for word in open("words.txt")]
    mm_url = "https://we6.talentsprint.com/wordle/game/"
    register_url = mm_url + "register"
    creat_url = mm_url + "create"
    guess_url = mm_url + "guess"

    def __init__(self, name: str):
        self.session = rq.session()
        register_dict = {"mode": "wordle", "name": name}
        reg_resp = self.session.post(MMBot.register_url, json=register_dict)
        self.me = reg_resp.json()['id']
        creat_dict = {"id": self.me, "overwrite": True}
        self.session.post(MMBot.creat_url, json=creat_dict)

        self.choices = [w for w in MMBot.words[:]]
        random.shuffle(self.choices)

    def play(self) -> str:
        def post(choice: str) -> tuple[str, bool]:
            guess = {"id": self.me, "guess": choice}
            response = self.session.post(MMBot.guess_url, json=guess)
            rj = response.json()

            feedback = rj["feedback"]
            status = "win" in rj["message"]
            return feedback, status

        choice = random.choice(self.choices)
        self.choices.remove(choice)
        feedback, won = post(choice)
        tries = [f'{choice}:{feedback}']

        while not won:
            if DEBUG:
                print(choice, feedback, self.choices[:10])
            self.update(choice, feedback)
            choice = random.choice(self.choices)
            self.choices.remove(choice)
            feedback, won = post(choice)
            tries.append(f'{choice}:{feedback}')
        print("Secret is", choice, "found in", len(tries), "attempts")
        print("Route is:", " => ".join(tries))
  

    def update(self, choice: str, feedback: str):
        def matches_feedback(word: str, choice: str, feedback: str) -> bool:
            for i in range(len(choice)):
                if feedback[i] == 'G' and word[i] != choice[i]:
                    return False
                elif feedback[i] == 'Y' and (word[i] == choice[i] or choice[i] not in word):
                    return False
                elif feedback[i] == 'R' and choice[i] in word:
                    return False
            return True
        
        self.choices = [w for w in self.choices if matches_feedback(w, choice, feedback)]



game = MMBot("CodeShifu")
game.play()
